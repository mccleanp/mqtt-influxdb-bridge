#!/usr/bin/env node

const fs = require('fs')
const yaml = require('js-yaml')
const mqtt = require('mqtt')
const moment = require('moment')
const mqtt_regex = require("mqtt-regex")
const mapValues = require('lodash.mapvalues')
const Influx = require('influx')

const filename = process.argv[2]

if ( !filename ) {
    console.warn(`USAGE: mqtt-influxdb-bridge CONFIG_FILE`);
    process.exit(1);
}

const data = fs.readFileSync(filename, 'utf8');
const config = yaml.load(data);

const influx = new Influx.InfluxDB({
 host: config.influxdb.host,
 database: config.influxdb.database
})

function on_connect() {
    console.log("Connected to MQTT")
    config.points.forEach(point => {
        const mregex = mqtt_regex(point.topic)
        client.subscribe(mregex.topic)
    });
}

function evaluateValues(o, params) {
    return mapValues(o,
        expr => eval(`({${Object.keys(params)}}) => ${expr}`)(params))
}

function on_message(topic, message) {
    try {
        const payload = JSON.parse(message.toString());
        config.points.forEach(point => {
            const measurement = point.measurement
            const mregex = mqtt_regex(point.topic)
            const match = mregex.exec(topic);
            if ( match ) {
                const params = { topic, ...match, ...payload };
                const timestamp = point.timestamp ? moment(params[point.timestamp]).toDate() : null;
                const fields = evaluateValues(point.fields, params);
                const tags = evaluateValues(point.tags, params);
                console.log(timestamp, fields, tags)
                influx.writePoints([{
                    timestamp,
                    measurement,
                    tags,
                    fields,
                }])
            }
        });
    } catch (e) {
        console.warn("Error parsing MQTT message",
            topic,
            message.toString(),
            e.message);
    }
}

const client = mqtt.connect({
    host: config.mqtt.host,
    port: config.mqtt.port,
    username: config.mqtt.user,
    password: config.mqtt.password
})

client.on('connect', on_connect);
client.on('message', on_message);